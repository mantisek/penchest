# How to enumerate s3

The following text is just some notes I've taken on how to enumerate s3


## Setup

On linux:

`sudo apt install awscli`

and then

`aws configure`

to set everything up. I'm using the following that I snagged online:

```
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```

test to see if you've got a connection to the bucket

`aws s3 ls s3://example-bucket/`

If you do it right (you may need to fool about with where you put your domains)
you should get a response like

`An error occured (InvalidAccessKeyID) when calling the ListObjectsV2 operation: The AWS Access Key Id you provided does not exist in our records.`

It's okay that it's an error, we're just seeing if the bucket is there.

## Enumerate tables, potentially find creds

Figure out what tables you have to work with, if any.

`aws dynamodb list-tables --endpoint-url http://example-bucket/`

which might produce an output like

```
{
    "TableNames": [
	"users"
    ]
}
```

and then to enumerate that table you'd type

`aws dynamodb scan --table-name users --endpoint-url http://example-bucket/`

Which might give you a list of users and passwords.

You can find more information from:

https://docs.aws.amazon.com/cli/latest/reference/dynamodb/scan.html#examples
https://stackoverflow.com/questions/60987653/dynamodb-local-web-shell-not-displaying-tables




