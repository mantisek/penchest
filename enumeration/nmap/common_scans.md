# NMAP (Common scans that I use)

## TCP

### Quick

`nmap -sC -sV -vv -oA quick <ip>`

### Full

`nmap -sC -sV -p- -vv -oA full <ip>`

## UDP

### Quick

`nmap -sU -sV -vv -oA quick_udp <ip>`

Full not really neccesary


## Port knocking

`for x in <<number set>>; do nmap -Pn --host_timeout 201 --max-retries 0 -p $x <ip>; done`


