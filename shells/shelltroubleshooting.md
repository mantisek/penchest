# Shell Troubleshooting

## General

1. Is it the right kind of shell? What does 'the right kind of shell' mean in this context?

2. Is it staged? Does it need to be?

3. Could AV be picking it up? Check to see if it keeps disappearing but other files don't.


## Netcat

1. Is the netcat you're using a version of netcat that has the -e option?

2. Is the port blocked?

3. Triple check your commands.

4. Dive deeper or back off.

## Webshells

1. Do you need ASP or ASPX?

2. Does JSP work?

3. Try a PHP shell.

4. Maybe you don't need an actual web shell, maybe you need a regular payload with just a web shell file extension

## Windows

1. If it's timing out, you may be piggy backing some service. You either need to migrate, or use this shell to open another one.

## Interesting tricks

Credit goes to twitter user @NinjaParanoid

```
If you can't listen on port 80 during a bind shell, try adding the URI '/Temporary_Listen_Address/' to ur listener. Magic! You don't need administrative privileges to listen on port 80 on Windows anymore 
```

<img src="https://pbs.twimg.com/media/EY7Zow9U8AE1uF9?format=png&name=large"
     alt="NinjaParanoid's Example"
     style="float: left; margin-right: 10px;" />

