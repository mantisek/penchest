# PenChest
(This repo is sporadically updated. https://www.revshells.com is a great resource that is way better than this repo.)

This repo is where I store resources I need and use for pentesting

Everything in this repo is something I've personally used and understand, it's not just a collection of stuff ripped from the web (for example, I know there's socat shells but I've never used one, and there's a lot of powershell shells out there but I haven't gotten one to work yet, usually because the quality of the RCE I'm working with for a particular box and I give up trying to escape any number of characters that are included in an incredibly long powershell one liner). If you're interviewing me (or thinking about interviewing me), you can ask me about any of these and I can tell you when I've used them (provided I'm not under NDA).  I have varying levels of depth of understanding for each thing.

Another reason I've decided to do these things this way, is that sometimes in infosec people play the game of telephone and just copy off of each other without verifying that something has worked. I want to build a reputation of someone you can count on to not be a BSer.

