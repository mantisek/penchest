# MSSQL General Tips


## Basic Syntax

List Databases

`mysql> SHOW DATABASES;`

`mysql> SHOW SCHEMAS;`

`mysql> USE <database name>`

`mysql> show tables;`

`mysql> select * from <table>`

## Interacting with an exposed MSSQL server

How to log in to an MSSQL server with sqsh, and then also execute commands.

`sqsh -S <ip> -U <username> -P <password>`

Once you're in, you can execute commands with

`exec xp_cmdshell 'whoami'`
`go`

Giving yourself a powershell shell is difficult. If you have system just make an admin or something.  Someone let me know what the easiest way to get a reverse shell out of this path.
