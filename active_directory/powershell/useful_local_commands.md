#Useful Local Commands

These are commands that are useful for your local machine, and aren't related in context to AD.

## What arch?

`$Env:PROCESSOR_ARCHITECTURE`

## Switch to x64 powershell, if you're on x86

`%SystemRoot%\sysnative\WindowsPowerShell\v1.0\powershell.exe`



