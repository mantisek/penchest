# Common cracking formats and how to crack them

## NTLM Hashes

`john --format=NT --rules -w=/usr/share/wordlists/rockyou.txt hashfile.txt`

## /etc/passwd /etc/shadow

`unshadow passwd.txt shadow.txt > passwords.txt`

`john --wordlist=/usr/share/wordlists/rockyou.txt passwords.txt`

